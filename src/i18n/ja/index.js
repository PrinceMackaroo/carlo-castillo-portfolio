// This is just an example,
// so you can safely delete all default props below

import date from './date';

export default {
  ...date,
  lbl_hi: "私は $[name] です。",
  lbl_aFrontEndDeveloper:"フロントエンド開発者。",
  lbl_imAlsoText:"魅力的な UI とシームレスなユーザー エクスペリエンスを作成する際に、RESTful API の実装や詳細志向の人員などのバックエンド開発にも関与しています。",

  navbar:{
    lbl_lightMode: "ライトモード",
    lbl_darkMode: "ダークモード",
    lbl_translate: "翻訳"
  },
  home:{
    lbl_skills: 'スキル',
    lbl_languagesFrameworksIUsed: '言語とフレームワーク',
    lbl_websites: 'ウェブサイト',
    lbl_discoverWebsites: `私がフロントエンド開発者として取り組んできた Web サイトのコレクションを発見します。`,
    lbl_someOfMyWorks:` 私の作品の中には公に紹介できないものもあります。追加の例を確認したり、私の幅広い経験について知りたい場合は、私にメッセージを送ってください `,
    lbl_here:'ここです。',

    lbl_skillsコンテンツ:`
    4 年以上の経験を持つ<b>フロントエンド開発者</b>であり、<b>Vue、React、Quasar</b> フレームワークおよびマルチプラットフォーム展開を専門としています。
    <br/><br/>私は独立した作業環境と共同作業環境の両方に優れています。
    私の専門知識には、<b>アジャイル手法</b>を活用して、ブロックチェーンや NFT などのテクノロジーをウェブサイトに統合することが含まれます。
     バックエンド API の作成と管理、データベース <b>(MySQL、SQLite、OracleDB)</b> の操作、UAT と運用環境の展開の処理、モバイル アプリケーションの展開の促進、Git リポジトリとバージョン管理システムの適切な管理。`,

    lbl_whyHireMe: 'なぜ私を として雇うのか ',
    lbl_frontEndDeveloper: 'フロントエンド開発者?',
   
    lbl_cleanMaintanableCode: 'クリーンで保守可能なコード',
    lbl_cleanMaintanableCodeContent: 'KISS、DRY、YAGNI などの業界のベスト プラクティスに従い、コードが理解しやすく、変更し、拡張しやすいようにしています。',
   
    lbl_ownedDesign: 'レスポンシブデザイン',
    lbl_ownedDesignContent: 'ユーザー エクスペリエンスを向上させるために、さまざまなデバイスや画面サイズにシームレスに適応する Web サイトを構築しています。',
 
    lbl_collaborationTeamwork: 'コラボレーションとチームワーク',
    lbl_collaborationTeamworkContent: '私は部門を超えたコラボレーションに優れ、効果的にコミュニケーションをとり、プロジェクトの成功に貢献し、必要に応じて問題を提起することを恐れません。',
 
    lbl_adaptableToEmergingTechnologies: '新しいテクノロジーに適応可能',
    lbl_adaptableToEmergingTechnologiesContent: '私は、強力なプログラミング基盤を活用して、新しいフレームワーク、言語、構造に自信を持って適応します。 進化し続ける業界において、革新的なソリューションで常に先を行きましょう。」',

    lbl_zenzic: 'ゼンジック',
     txt_zenzicContent: 'ドキュメント管理システム (DMS) と顧客維持管理 (CRM) の機能で構成される Web サイト アプリケーションは、企業がさまざまな顧客、クライアント、リードを追跡するために使用できます。',

    lbl_messages: '言語',
    lbl_frameworks: 'フレームワーク',
    lbl_other: 'その他'

  },
  footer:{
    lbl_contactMe: '連絡してください!',
    lbl_sendAMessage:'メッセージを送信',
    lbl_thisWebsiteHasBeenCreated: 'この Web サイトは、Quasar + VueJS + Gitlab Pages を使用して Magbanua 王子によって作成されました' ,
    lbl_viewGitlabProjectHere: 'Gitlab プロジェクトを表示できます ',
    lbl_here: 'ここ',
    lbl_interestedInHiringMe: '興味がありますか? ぜひご意見を伺い、どのように協力できるか話し合っていきたいと思います。',
  },
  website:{
    lbl_rserv: 'rserv.io',
    txt_rservContent: `ユーザーに既存の NFT を販売および取引する機会と、今後の NFT リリース用のホワイトリスト スポットを確保する機会を提供するマーケットプレイス Web サイト。`,
 
    lbl_uandworld: 'U&ワールド',
    txt_uandworldContent: `ウクライナのさまざまな市場セグメントの再建に焦点を当てた募金プロジェクトの作成と寄付を可能にする電子商取引プラットフォーム`,
 
    lbl_daypay: 'デイペイ',
    txt_daypayContent: `通貨取引のためのオンライン プラットフォーム。ユーザーがシームレスにデジタル通貨の売買や、現金への変換を行うことができます。`,

    lbl_daypayAdmin: 'DayPay 管理者',
    txt_daypayAdminContent: `DayPay Web サイトのコンテンツおよびシステム管理 Web サイトは、レポートと構成で構成されます。`,

    lbl_perfectCurve: 'PTC',
    txt_perfectCurveContent: `暗号通貨を使用して NFT を購入し、ステーキングして PTC を獲得できるオンライン NFT マーケットプレイスおよびステーキング プラットフォーム。`,

    lbl_wprophecy:'WProphecy',
    txt_wprophecyContent:'この Web サイトで、ユーザーはアカウントを作成し、特定の国間の将来の戦争に賭ける暗号通貨に参加できます。 また、毎日の成果を獲得して報酬を受け取る機会もあります。',

    lbl_wprophecyAdmin: 'WProphecy 管理者',
    txt_wprophecyAdminContent: 'これは WProphecy の管理 Web サイトであり、管理者にユーザー データへの広範な制御とアクセスを提供します。 設定を構成でき、包括的なレポート機能を提供します。',
   
    lbl_preview: 'プレビュー'
  },

  contact:{
    lbl_email: '電子メール',
    lbl_name: '名前',
    lbl_message: 'メッセージ',

    lbl_sendMessage:'メッセージを送信',

    lbl_thankYouForReachingOut: 'ご連絡いただきありがとうございます。以下からお問い合わせを送信していただけます!'
  }
};
