// This is just an example,
// so you can safely delete all default props below

import date from './date';

export default {
  ...date,lbl_hi: "저는 $[name]입니다.",
  lbl_aFrontEndDeveloper:"프론트 엔드 개발자입니다.",
  lbl_imAlsoText:"또한 매력적인 UI와 원활한 사용자 경험을 만들 때 RESTful API 구현 및 세부 지향적인 사람을 포함하여 백엔드 개발에 관여합니다.",

  navbar:{
    lbl_lightMode: "조명 모드",
    lbl_darkMode: "어두운 모드",
    lbl_translate: "번역하기"
  },
  home:{
    lbl_skills: '기술',
    lbl_languagesFrameworksIUsed: '언어 및 프레임워크',
    lbl_websites: '웹사이트',
    lbl_discoverWebsites: `프론트 엔드 개발자로 작업한 웹사이트 모음을 검색합니다. `,
    lbl_someOfMyWorks:`내 작품 중 일부는 공개적으로 전시할 수 없습니다. 추가 예시를 보고 내 광범위한 경험에 대해 알아보려면 메시지를 보내주세요`,
    lbl_here:'여기.',

    lbl_skillsContent:`
    <b>Vue, React 및 Quasar</b> 프레임워크와 다중 플랫폼 배포를 전문으로 하는 4년 이상의 경력을 가진 <b>프론트 엔드 개발자</b>입니다.
    <br/> <br/> 나는 독립적이고 협력적인 작업 환경 모두에서 뛰어납니다.
    제 전문 분야에는 블록체인 및 NFT와 같은 기술을 웹사이트에 통합하고 <b>애자일 방법론</b>을 활용하는 것이 포함됩니다.
     백엔드 API 생성 및 관리, 데이터베이스<b>(MySQL, SQLite, OracleDB)</b> 작업, UAT 및 프로덕션 배포 처리, 모바일 애플리케이션 배포 촉진, git 리포지토리 및 버전 제어 시스템 능숙하게 관리`,

    lbl_whyHireMe: '나를 고용해야 하는 이유',
    lbl_frontEndDeveloper: '프론트엔드 개발자?',
   
    lbl_cleanMaintanableCode: '깨끗하고 유지 관리 가능한 코드',
    lbl_cleanMaintanableCodeContent: 'KISS, DRY 및 YAGNI와 같은 업계 모범 사례를 따라 이해, 수정 및 확장하기 쉬운 코드를 보장합니다.',
   
    lbl_responsiveDesign: '반응형 디자인',
    lbl_responsiveDesignContent: '향상된 사용자 경험을 위해 다양한 기기와 화면 크기에 매끄럽게 적응하는 웹사이트를 구축합니다.',
 
    lbl_collaborationTeamwork: '협업 및 팀워크',
    lbl_collaborationTeamworkContent: '나는 교차 기능 협업, 효과적으로 의사 소통, 프로젝트 성공에 기여하고 필요한 경우 문제 제기를 두려워하지 않는 데 탁월합니다.',
 
    lbl_adaptableToEmergingTechnologies: '신기술에 적응 가능',
    lbl_adaptableToEmergingTechnologiesContent: '강력한 프로그래밍 기반을 활용하여 새로운 프레임워크, 언어 및 구조에 자신 있게 적응합니다. 끊임없이 진화하는 산업에서 혁신적인 솔루션으로 앞서 나가십시오.',

    lbl_languages: '언어',
    lbl_frameworks: '프레임워크',
    lbl_other: '기타'

  },
  footer:{
    lbl_contactMe: '저에게 연락하세요!',
    lbl_sendAMessage:'메시지 보내기',
    lbl_thisWebsiteHasBeenCreated: '이 웹사이트는 Quasar + VueJS + Gitlab Pages를 사용하여 Prince Magbanua에 의해 만들어졌습니다.' ,
    lbl_viewGitlabProjectHere: 'Gitlab 프로젝트를 볼 수 있습니다',
    lbl_here: '여기',
    lbl_interestedInHiringMe: '관심이 있습니까? 귀하의 의견을 듣고 어떻게 협력할 수 있는지 논의하고 싶습니다.',
  },
  websites:{
    lbl_rserv: 'rserv.io',
    txt_rservContent: `사용자에게 기존 NFT를 판매하고 거래할 수 있는 기회와 향후 NFT 출시를 위한 안전한 화이트리스트 자리를 제공하는 마켓플레이스 웹사이트입니다.`,
 
    lbl_uandworld: '유앤월드',
    txt_uandworldContent: `우크라이나의 다양한 시장 부문 재건에 초점을 맞춘 기금 모금 프로젝트를 만들고 기부할 수 있는 전자 상거래 플랫폼`,
 
    lbl_daypay: '데이페이',
    txt_daypayContent: `통화 거래를 위한 온라인 플랫폼으로 사용자가 원활하게 디지털 통화를 사고 팔고 현금으로 또는 그 반대로 전환할 수 있습니다.`,

    lbl_daypayAdmin: '데이페이 관리자',
    txt_daypayAdminContent: `데이페이 웹사이트의 콘텐츠 및 시스템 관리 웹사이트로 보고서 및 구성으로 구성되어 있습니다.`,

    lbl_perfectCurve: 'PTC',
    txt_perfectCurveContent: `암호화폐를 사용하여 NFT를 구매하고 PTC를 얻기 위해 스테이킹할 수 있는 온라인 nft 마켓플레이스 및 스테이킹 플랫폼입니다.`,

    lbl_wprophecy:'W예언',
    txt_wprophecyContent:'이 웹사이트에서 사용자는 계정을 만들고 특정 국가 간의 향후 전쟁에 대한 암호화폐 베팅에 참여할 수 있습니다. 또한 일일 업적을 달성하고 보상을 청구할 수 있는 기회도 있습니다.',

    lbl_wprophecyAdmin: 'WProphecy 관리자',
    txt_wprophecyAdminContent: '이것은 관리자에게 사용자 데이터에 대한 광범위한 제어 및 액세스를 제공하는 WProphecy의 관리 웹사이트입니다. 설정 구성을 허용하고 포괄적인 보고 기능을 제공합니다.',
    
    lbl_zenzic: '젠지크',
    txt_zenzicContent: '문서 관리 시스템(DMS)과 고객 유지 관리(CRM) 기능으로 구성된 웹 사이트 응용 프로그램으로 회사에서 다양한 고객, 클라이언트 및 리드를 추적하는 데 사용할 수 있습니다.',
    
    lbl_preview: '미리보기'
  },

  contact:{
    lbl_email: '이메일',
    lbl_name: '이름',
    lbl_message: '메시지',

    lbl_sendMessage:'메시지 보내기',

    lbl_thankYouForReachingOut: '연락해 주셔서 감사합니다. 아래에서 문의를 보내주실 수 있습니다!'
  }
};
