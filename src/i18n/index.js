import enUS from './en-US'
import cn from './cn'
import ja from './ja'
import ko from './ko'
import th from './th'

export default {
  'en-US': enUS,
  'cn': cn,
  'ja': ja,
  'ko': ko,
  'th': th
}
