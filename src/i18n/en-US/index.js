// This is just an example,
// so you can safely delete all default props below

import date from './date';

export default {
  ...date,
  lbl_hi: "I'm $[name].",
  lbl_aFrontEndDeveloper:"A Front-end Developer.",
  lbl_imAlsoText:"I also meddle with backend development, including RESTful API implementations and a detail-oriented person when creating engaging UI and seamless user experience.",

  navbar:{
    lbl_lightMode: "Light Mode",
    lbl_darkMode: "Dark Mode",
    lbl_translate: "Translate"
  },
  home:{
    lbl_skills: 'Skills',
    lbl_languagesFrameworksIUsed: 'Languages & Frameworks',
    lbl_websites: 'Websites',
    lbl_discoverWebsites: `Discover a collection of websites I've worked on as a Front-end Developer.`,
    lbl_someOfMyWorks:`Some of my works cannot be publicly showcased, please message me to see additional examples and learn about my extensive experience `,
    lbl_here:'here.',

    lbl_skillsContent:`
    A <b>Front-end Developer</b> with 4+ years of experience specializing in <b>Vue, React, and Quasar</b> frameworks as well as multi-platform deployment .
    <br/> <br/> I excel in both independent and collaborative work environments. 
    My expertise includes integrating technologies like blockchain and NFTs into websites, utilizing <b>Agile methodologies</b>,
     creating and managing Back-end APIs, working with databases <b>(MySQL, SQLite, OracleDB)</b>, handling UAT and production deployment, facilitating mobile application deployment, and proficiently managing git repositories and version-control systems.`,

    lbl_whyHireMe: 'Why Hire Me as a ',
    lbl_frontEndDeveloper: 'Front-end Developer?',
    
    lbl_cleanMaintanableCode: 'Clean, Maintainable Code',
    lbl_cleanMaintanableCodeContent: 'I follow industry best practices like KISS, DRY, and YAGNI to ensure code that is easy to understand, modify, and extend.',
    
    lbl_responsiveDesign: 'Responsive design',
    lbl_responsiveDesignContent: 'I build websites that adapt seamlessly to different devices and screen sizes for enhanced user experiences.',
  
    lbl_collaborationTeamwork: 'Collaboration and teamwork',
    lbl_collaborationTeamworkContent: 'I excel in cross-functional collaboration, effectively communicating, contributing to project success and not afraid to raise issues if necessary.',
  
    lbl_adaptableToEmergingTechnologies: 'Adaptable to New Technologies',
    lbl_adaptableToEmergingTechnologiesContent: 'I confidently adapt to new frameworks, languages, and structures, leveraging my strong programming foundation. Stay ahead with innovative solutions in an ever-evolving industry.',

    lbl_languages: 'Languages',
    lbl_frameworks: 'Frameworks',
    lbl_other: 'Other'

  },
  footer:{
    lbl_contactMe: 'Contact Me!',
    lbl_sendAMessage:'Send A Message',
    lbl_thisWebsiteHasBeenCreated: 'This website has been created by Prince Magbanua using Quasar + VueJS + Gitlab Pages' ,
    lbl_viewGitlabProjectHere: 'You can view the Gitlab Project ',
    lbl_here: 'Here',
    lbl_interestedInHiringMe: 'Interested? I\'d love to hear from you and discuss how we can collaborate.',
  },
  websites:{
    lbl_rserv: 'rserv.io',
    txt_rservContent: `A marketplace website that provides users with the opportunity to sell and trade their existing NFTs, as well as secure whitelist spots for upcoming NFT releases.`,
  
    lbl_uandworld: 'U&World',
    txt_uandworldContent: `An E-Commerce platform, that allows creating and donating fund-raising projects focused in rebuilding Ukraine's different market segments`,
  
    lbl_daypay: 'DayPay',
    txt_daypayContent: `An online platform for currency trading, enabling users to seamlessly engage in the buying or selling of digital currencies and converting them into cash or vice versa.`,

    lbl_daypayAdmin: 'DayPay Admin',
    txt_daypayAdminContent: `The content and system management website for the DayPay website, consists of the reports and configurations.`,

    lbl_perfectCurve: 'PTC',
    txt_perfectCurveContent: `An online nft marketplace and staking platform where you can buy NFTs using cryptocurrencies and stake them to earn PTC.`,

    lbl_wprophecy:'WProphecy',
    txt_wprophecyContent:'On this website, users can create an account and engage in cryptocurrency betting for future wars between specific countries. They also have the opportunity to earn daily achievements and claim rewards.',

    lbl_wprophecyAdmin: 'WProphecy Admin',
    txt_wprophecyAdminContent: 'This is the admin website for WProphecy, providing administrators with extensive control and access to user data. It allows configuration of settings and offers comprehensive reporting features.',
    
    lbl_zenzic: 'Zenzic',
    txt_zenzicContent: 'A Website Application that consists of a Document Management System (DMS) and features for Customer Retention Management (CRM), can be used by companies to keep track of different customers, clients and leads.',

    lbl_preview: 'Preview'
  },

  contact:{
    lbl_email: 'Email',
    lbl_name: 'Name',
    lbl_message: 'Message',

    lbl_sendMessage:'Send Message',

    lbl_thankYouForReachingOut: 'Thanks for reaching out, you can send me an Inquiry down below!'
  }
};
