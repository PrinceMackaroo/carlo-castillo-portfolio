// This is just an example,
// so you can safely delete all default props below

import date from './date';

export default {
  ...date,lbl_hi: "我是 $[name]。",
  lbl_aFrontEndDeveloper:"前端开发人员。",
  lbl_imAlsoText:"我还参与了后端开发,包括 RESTful API 实现和创建引人入胜的 UI 和无缝用户体验时注重细节的人员。",

  navbar:{
    lbl_lightMode: "光照模式",
    lbl_darkMode: "深色模式",
    lbl_translate: "翻译"
  },
  home:{
    lbl_skills: '技能',
    lbl_languagesFrameworksIUsed: '语言和框架',
    lbl_websites: '网站',
    lbl_discoverWebsites:`发现我作为前端开发人员工作过的网站集合。 `,
    lbl_someOfMyWorks:`我的一些作品无法公开展示,请给我留言以查看更多示例并了解我的丰富经验`,
    lbl_here:'这里。',

    lbl_skillsContent:`
    <b>前端开发人员</b>,拥有 4 年以上的经验,专注于 <b>Vue、React 和 Quasar</b> 框架以及多平台部署。
    <br/><br/> 我在独立和协作工作环境中都表现出色。
    我的专长包括将区块链和 NFT 等技术集成到网站中,利用<b>敏捷方法</b>,
     创建和管理后端 API、使用数据库<b>（MySQL、SQLite、OracleDB）</b>、处理 UAT 和生产部署、促进移动应用程序部署以及熟练管理 git 存储库和版本控制系统。`,

    lbl_whyHireMe:'为什么雇用我',
    lbl_frontEndDeveloper: '前端开发人员？',
   
    lbl_cleanMaintanableCode: '干净、可维护的代码',
    lbl_cleanMaintanableCodeContent: '我遵循 KISS、DRY 和 YAGNI 等行业最佳实践,以确保代码易于理解、修改和扩展。',
   
    lbl_responsiveDesign: '响应式设计',
    lbl_responsiveDesignContent: '我构建的网站可以无缝适应不同的设备和屏幕尺寸,以增强用户体验。',
 
    lbl_collaborationTeamwork: '协作和团队合作',
    lbl_collaborationTeamworkContent: '我擅长跨职能协作,有效沟通,为项目成功做出贡献,并且在必要时不怕提出问题。',
 
    lbl_adaptableToEmergingTechnologies: '适应新技术',
    lbl_adaptableToEmergingTechnologiesContent:'我自信地适应新的框架、语言和结构,利用我强大的编程基础。 在不断发展的行业中保持创新解决方案的领先地位。',

    lbl_languages: '语言',
    lbl_frameworks: '框架',
    lbl_other: '其他'

  },
  footer:{
    lbl_contactMe: '联系我！',
    lbl_sendAMessage:'发送消息',
    lbl_thisWebsiteHasBeenCreated: '这个网站是由 Prince Magbanua 使用 Quasar + VueJS + Gitlab Pages 创建的' ,
    lbl_viewGitlabProjectHere: '可以查看Gitlab项目',
    lbl_here: '这里',
    lbl_interestedInHiringMe:'有兴趣吗？ 我很乐意听取您的意见并讨论我们如何合作。',
  },
  websites:{
    lbl_rserv: 'rserv.io',
    txt_rservContent:`一个市场网站,为用户提供出售和交易其现有 NFT 的机会,以及为即将发布的 NFT 版本提供安全的白名单点。`,
 
    lbl_uandworld: '你&世界',
    txt_uandworldContent:`一个电子商务平台,允许创建和捐赠专注于重建乌克兰不同细分市场的筹款项目`,
 
    lbl_daypay: '日付',
    txt_daypayContent:`一个在线货币交易平台,使用户能够无缝地参与数字货币的买卖并将其转换为现金,反之亦然。`,

    lbl_daypayAdmin: 'DayPay 管理员',
    txt_daypayAdminContent:`DayPay 网站的内容和系统管理网站,包括报告和配置。`,

    lbl_perfectCurve: 'PTC',
    txt_perfectCurveContent:`一个在线 nft 市场和质押平台,您可以在其中使用加密货币购买 NFT 并将其质押以赚取 PTC。`,

    lbl_wprophecy:'WProphecy',
    txt_wprophecyContent:'在这个网站上,用户可以创建一个帐户并为特定国家之间的未来战争进行加密货币投注。 他们还有机会获得每日成就并领取奖励。',

    lbl_wprophecyAdmin: 'WProphecy 管理员',
    txt_wprophecyAdminContent: '这是 WProphecy 的管理网站,为管理员提供对用户数据的广泛控制和访问。 它允许配置设置并提供全面的报告功能。',
   
    lbl_zenzic: '曾齐克',
     txt_zenzicContent: '一个由文档管理系统 (DMS) 和客户保留管理 (CRM) 功能组成的网站应用程序，公司可以使用它来跟踪不同的客户、客户和潜在客户。',

    lbl_preview: '预览'
  },

  contact:{
    lbl_email: '电子邮件',
    lbl_name: '姓名',
    lbl_message: '消息',

    lbl_sendMessage:'发送消息',

    lbl_thankYouForReachingOut:"感谢您联系我们,您可以在下方向我发送询问！"
  }
  
};